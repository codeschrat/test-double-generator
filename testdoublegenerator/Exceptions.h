#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>


class NotAMemberFunction : public std::exception {
  const char* what() const throw() override
  {
    return "Tried to resolve a pointer to something that is not a member function.";
  }
};

class NoFunctionRegistered : public std::exception {
  const char* what() const throw() override
  {
    return "No function has been registered for this member.";
  }
};

#endif // EXCEPTIONS_H
