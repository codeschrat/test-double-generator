def arguments_string(arg_count):
  result = ""
  for i in range(arg_count):
    if i != 0:
      result += ", "
    result += "A" + str(i)
  return result

def functor_template_argument(arg_count, return_type_in_template, C_to_include=""):
  if return_type_in_template == "void":
    ret_type_in_template = ""
  else:
    ret_type_in_template = return_type_in_template

  result = "<"
  if arguments_string(arg_count) != "":
    result += arguments_string(arg_count)

  if result != "<" and ret_type_in_template != "":
    result += ", "
  result += ret_type_in_template

  if C_to_include != "":
    if result != "<":
      result += ", C"
    else:
      result += "C"
  result += ">"

  if result == "<>":
    result = ""

  return result


def write_extend_with_class(file, max_arg_count, return_type_in_template,
                            return_type_in_function,
                            return_type_in_functor_name, const_qualifier):
    for arg_count in range(max_arg_count+1):
      file.write("    template <class C" + return_type_in_template)
      for i in range(arg_count):
        file.write(", class A" + str(i))
      file.write(">\n")
      
      file.write("    void extend (" + return_type_in_function + " (BaseClass::*memberFunction)(")
      file.write(arguments_string(arg_count))
      file.write(") " + const_qualifier + ", C& object, " + return_type_in_function + " (C::*function)(")
      file.write(arguments_string(arg_count))
      file.write(") " + const_qualifier + ") {\n")
      
      file.write("        extend_internal(memberFunction, static_cast<Func" + str(arg_count) + return_type_in_functor_name)
      file.write(functor_template_argument(arg_count, return_type_in_function))
      file.write("* >(new Func" + str(arg_count) + return_type_in_functor_name + "_impl")
      file.write(functor_template_argument(arg_count, return_type_in_function, ", C"))
      file.write("(object, function)));\n")
      
      file.write("    }\n")
      file.write("\n")
    file.write("\n")


def write_extend_static(file, max_arg_count, return_type_in_template,
                        return_type_in_function, return_type_in_functor_name,
                        const_qualifier):
    for arg_count in range(max_arg_count+1):
      template_argument = return_type_in_template
      for i in range(arg_count):
        if template_argument != "":
          template_argument += ", "
        template_argument += "class A" + str(i)
      if template_argument != "":
        if not template_argument.startswith("class"):
          template_argument = "class " + template_argument
        file.write("    template <" + template_argument + ">\n")
      
      file.write("    void extend (" + return_type_in_function + " (BaseClass::*memberFunction)(")
      file.write(arguments_string(arg_count))
      file.write(") " + const_qualifier + ", " + return_type_in_function + " (*function)(")
      file.write(arguments_string(arg_count))
      file.write(")) {\n")
      
      file.write("        extend_internal(memberFunction, static_cast<Func" + str(arg_count) + return_type_in_functor_name)
      file.write(functor_template_argument(arg_count, return_type_in_template))
      file.write("* >(new Func" + str(arg_count) + return_type_in_functor_name + "_impl_static")
      file.write(functor_template_argument(arg_count, return_type_in_template))
      file.write("(function)));\n")
      
      file.write("    }\n")
      file.write("\n")
    file.write("\n")


def extend_with_class_without_return_value(file, max_arg_count, const_qualifier):
    return_type_in_template = ""
    return_type_in_function = "void"
    return_type_in_functor_name = "c" if const_qualifier == "const" else ""
    write_extend_with_class(file, max_arg_count, return_type_in_template,
                            return_type_in_function,
                            return_type_in_functor_name, const_qualifier)

def extend_with_class_with_return_value(file, max_arg_count, const_qualifier):
    return_type_in_template = ", class R"
    return_type_in_function = "R"
    return_type_in_functor_name = "wRc" if const_qualifier == "const" else "wR"
    write_extend_with_class(file, max_arg_count, return_type_in_template,
                            return_type_in_function,
                            return_type_in_functor_name, const_qualifier)

def extend_static_without_return_value(file, max_arg_count, const_qualifier):
    return_type_in_template = ""
    return_type_in_function = "void"
    return_type_in_functor_name = "c" if const_qualifier == "const" else ""
    write_extend_static(file, max_arg_count, return_type_in_template,
                        return_type_in_function, return_type_in_functor_name,
                        const_qualifier)
    
def extend_static_with_return_value(file, max_arg_count, const_qualifier):
    return_type_in_template = "R"
    return_type_in_function = "R"
    return_type_in_functor_name = "wRc" if const_qualifier == "const" else "wR"
    write_extend_static(file, max_arg_count, return_type_in_template,
                        return_type_in_function, return_type_in_functor_name,
                        const_qualifier)



def write_disclosure(file, arguments):
    argumentsAsString = " ".join(arguments)
    file.write("// This file was generated by the command " + argumentsAsString + "\n")


def write_include_guard_top(file):
    file.write("#ifndef MOCK_WRAPPER_H\n")
    file.write("#define MOCK_WRAPPER_H\n")
    file.write("\n")


def write_include_guard_bottom(file):
    file.write("#endif // MOCK_WRAPPER_H\n")


def write_includes(file):
    file.write("#include \"Functors.h\"\n")
    file.write("\n")


def write_class_openning(file):
    file.write("template <class BaseClass>\n")
    file.write("class Mock : public BaseClass {\n")


def write_class_closing(file):
    file.write("};\n")
    file.write("\n")


def write_public_members(file):
    file.write("\n")
    file.write("public:\n")
    file.write("    static Mock* createMock();\n")
    file.write("    virtual ~Mock();\n")
    file.write("\n")


def write_protected_members(file):
    file.write("protected:\n")
    file.write("    template<class T, class U>\n")
    file.write("    void extend_internal(T memberFunction, U functor);\n")
    file.write("\n")
    file.write("    Mock();\n")
    file.write("    class Mock_impl;\n")
    file.write("    Mock_impl* d;\n")
    file.write("\n")


def write_private_members(file):
    file.write("private:\n")
    file.write("    Mock(Mock const& other);\n")
    file.write("    Mock& operator=(Mock const& other);\n")
    file.write("\n")


if __name__ == "__main__":
  import sys
  max_arg_count = int(sys.argv[1])
  output_file = sys.argv[2]
  with open(output_file, "w") as file:
    write_disclosure(file, sys.argv)
    write_include_guard_top(file)
    write_includes(file)

    write_class_openning(file)
    write_public_members(file)
    
    for const_qualifier in ["", "const"]:
        extend_with_class_without_return_value(file, max_arg_count, const_qualifier)
        extend_with_class_with_return_value(file, max_arg_count, const_qualifier)
        extend_static_without_return_value(file, max_arg_count, const_qualifier)
        extend_static_with_return_value(file, max_arg_count, const_qualifier)

    write_protected_members(file)
    write_private_members(file)
    write_class_closing(file)

    write_include_guard_bottom(file)