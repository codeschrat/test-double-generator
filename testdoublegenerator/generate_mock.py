import re


def string_before_match_contains_unbalanced_chevrons(string, match_position):
    string_portion_of_interest = string[0:match_position]
    number_of_left_chevrons = string_portion_of_interest.count("<")
    number_of_right_chevrons = string_portion_of_interest.count(">")
    return number_of_left_chevrons != number_of_right_chevrons


def find_arguments(arguments_string):
    if arguments_string == "":
        return []

    regexToFindCommas = re.compile(r",")
    matches = regexToFindCommas.finditer(arguments_string)

    split_positions = []
    for match in matches:
        if string_before_match_contains_unbalanced_chevrons(arguments_string, match.start()):
            continue
        start = 0
        if len(split_positions) != 0:
            start = split_positions[-1][1] + 1
        split_positions.append((start, match.start()))
    if len(split_positions) != 0:
        split_positions.append((split_positions[-1][1] + 1,
                                len(arguments_string)))

    arguments = []
    if len(split_positions) == 0:
        arguments.append(arguments_string)
    else:
        for split_position in split_positions:
            arguments.append(arguments_string[split_position[0]:split_position[1]].strip())

    return arguments


def remove_compiler_directives(string):
  return re.sub(r"^#.*$","",string,flags=re.MULTILINE)

def remove_empty_lines(string):
  return re.sub(r"^\s*(\r|\n|\r\n|\n\r)","",string,flags=re.MULTILINE)

def remove_multi_line_comments(string):
  return re.sub(r"/\*([^\*]|\*(?!/))*\*/","",string)

def remove_single_line_comments(string):
  return re.sub(r"//.*$","",string,flags=re.MULTILINE)

def load_and_preprocess_file(path_to_file):
  content = ""
  with open(path_to_file) as file:
    line = file.readline()
    while line:
      if line.strip():
        content += line
      line = file.readline()
    content = remove_compiler_directives(content)
    content = remove_multi_line_comments(content)
    content = remove_single_line_comments(content)
    content = remove_empty_lines(content)
    return content


def load_file_into_string(path_to_file):
    with open(path_to_file) as file:
        content = file.read()
    return content


def find_class_name(string):
  token1 = re.search("class [^:{;]*[^:{](?=(:|{))",string)
  class_name = ""
  if token1:
    class_name = re.findall("[A-Za-z_0-9]+",token1.group(0))[-1]
  return class_name


def find_relevant_namespaces(string, class_to_mock):
    regex = "|".join(["(?<=namespace )[A-Za-z0-9_]+", "}", class_to_mock])
    tokens = re.findall(regex, string)
    relevant_namespaces = []
    for token in tokens:
        if token == class_to_mock:
            break
        if token == "}":
            del relevant_namespaces[-1]
        else:
            relevant_namespaces.append(token)
    return relevant_namespaces


def determine_namespace(string, class_to_mock):
  elements = find_relevant_namespaces(string, class_to_mock)
  namespace = ""
  for e in elements:
      if namespace == "":
          namespace = e
      else:
          namespace += "::" + e
  return namespace


def find_pure_virtual_functions(string):
    regex_to_find_pure_virtual_functions = \
        re.compile(r"(?:virtual )([\w\<, \>\:]+) +(\w+) *\(([\w\<\>\, \n\*\:=&]*)\)( )*(const)?(?: )*(?:= 0;)")
    list_of_token_tuples = regex_to_find_pure_virtual_functions.findall(string, re.MULTILINE)

    pure_virtual_functions = [drop_surplus_white_space(a_tuple) for a_tuple in list_of_token_tuples]

    return pure_virtual_functions


def drop_surplus_white_space(arg):
    if type(arg) == str:
        return re.sub("\n[ ]*", "", arg)
    return tuple([drop_surplus_white_space(token) for token in arg])


def find_typedefs(string):
    regex_to_find_typedefs = re.compile(r"(?<=\btypedef\b)[ \n]+[^;]+[ \n]+([A-Za-z0-9_]+)[ \n]*;")
    typedef_names = regex_to_find_typedefs.findall(string)
    return typedef_names


def find_structs(string):
    regex_to_find_structs = re.compile(r"(?<=\bstruct\b)[ \n]+([A-Za-z0-9_]+)[ \n]+[{:]")
    struct_names = regex_to_find_structs.findall(string)
    return struct_names


def find_enums(string):
    regex_to_find_enums = re.compile(r"(?<=\benum\b)[ \n]+([A-Za-z0-9_]+)[ \n]+{")
    enum_names = regex_to_find_enums.findall(string)
    return enum_names


def arguments_for_calling(arguments_string):
  argument_names = ""
  for argument in find_arguments(arguments_string):
    if argument_names != "":
      argument_names += ","
    if "=" in argument:
        argument = argument.split("=")[0].strip()
    argument_names += argument.split(" ")[-1]
  return argument_names

def types_for_templates(arguments_string):
  argument_types = ""
  for argument in find_arguments(arguments_string):
    if argument_types != "":
      argument_types += ","
    if "=" in argument:
        argument = argument.split("=")[0].strip()
    tokens = argument.split(" ")[:-1]
    tokens = [t for t in tokens if t != ""]
    argument_types += " ".join(tokens)
  return argument_types

def argument_and_return_types_string(arguments, return_type, const_qualifier):
  result = types_for_templates(arguments).replace(",","_").replace("*","p").replace("&","r").replace("<","_").replace(">","_").replace(" ","").replace(":","")
  if result == "":
    result = "void"
  if const_qualifier == "const":
      return_type = "c" + return_type
  result += "R" + return_type.replace("<","_").replace(">","_").replace(",","_").replace(" ","").replace(":","")
  return result

def extract_lookup_function_content(functions):
  content = {}
  for ret_type, func_name, arguments, _, const_qualifier in functions:
      ident_string = argument_and_return_types_string(arguments, ret_type, const_qualifier)
      if not ident_string in content:
        content[ident_string] = [(ret_type, func_name, arguments, const_qualifier)]
      else:
        content[ident_string] += [(ret_type, func_name, arguments, const_qualifier)]
  return content
      
def number_of_arguments(arg_types):
    return len(find_arguments(arg_types))


def returnType_part_of_functor_signature(returnType):
  if (returnType != "void"):
    return "wR"
  return ""

def functor_signature(returnType, numberOfArguments, argumentTypes, const_qualifier):
  signature = str(numberOfArguments)
  signature += returnType_part_of_functor_signature(returnType)
  if (const_qualifier == "const"):
      signature += "c"
  if (numberOfArguments > 0 or returnType != "void"):
    signature += "<"
  if (numberOfArguments > 0):
    signature += argumentTypes
  if (returnType != "void"):
    if (numberOfArguments):
      signature += ","
    signature += returnType
  if (numberOfArguments > 0 or returnType != "void"):
    signature += ">"
  return signature


def write_disclosure(file, arguments):
    argumentsAsString = " ".join(arguments)
    file.write("// This file was generated by the command " + argumentsAsString + "\n")


def write_includes(file, source_file):
    file.write("#include \"mock_wrapper.h\"\n")
    file.write("#include \"Functors.h\"\n")
    file.write("#include \"Exceptions.h\"\n")
    file.write("#include \"" + source_file + "\"\n")
    file.write("\n")


def write_using_declaration(file, namespace, class_name):
    file.write("using " + namespace + "::" + class_name + ";\n")
    file.write("\n")


def write_aliases(file, namespace, class_name, aliases):
    for alias in aliases:
        file.write("typedef " + namespace + "::" + class_name + "::" + alias + " " + alias + ";\n")
    file.write("\n")


def find_prerequisites(output_file, makefile_content):
    target = output_file.replace(".", r"\.")
    regex = r"(?<=^" + target + r": )[A-Za-z0-9_\-\. \t\\/\n]+?[^\\]\n"
    prerequisites_as_string = re.search(regex, makefile_content, re.MULTILINE).group(0)
    prerequisites = []
    for token in prerequisites_as_string.split():
        if token != "\\":
            prerequisites.append(token)
    return prerequisites


def find_direct_public_ancestors(content):
    direct_public_ancestors = []
    regex_to_find_base_specifiers = r"(?<=class)[A-Za-z0-9_ \n]+:[A-Za-z0-9_ ,\:\n]+"
    class_name_and_base_specifiers = re.search(regex_to_find_base_specifiers, content, re.MULTILINE)
    if class_name_and_base_specifiers:
        list_of_base_specifiers = class_name_and_base_specifiers.group(0).strip().split(":", 1)[1].split(",")
        for base_specifier in list_of_base_specifiers:
            regex_to_check_for_public_parent = re.compile(r"\bpublic\b")
            if regex_to_check_for_public_parent.search(base_specifier):
                base_specifier_including_namespace = re.split(r"[ ]+", base_specifier.strip())[-1]
                direct_public_ancestors.append(base_specifier_including_namespace)
    return direct_public_ancestors


def pick_relevant_ancestors(ancestors_per_class, class_name):
    ancestors = []

    if class_name in ancestors_per_class:
        direct_ancestors = ancestors_per_class.get(class_name).get("ancestors")
        for direct_ancestor in direct_ancestors:
            ancestors.append(direct_ancestor)

            next_generation_ancestors = pick_relevant_ancestors(ancestors_per_class, direct_ancestor)
            for next_generation_ancestor in next_generation_ancestors:
                ancestors.append(next_generation_ancestor)

    return set(ancestors)


def pick_ancestors_files(included_files, class_name):
    classes = {}
    for file in included_files:
        file_content = load_and_preprocess_file(file)

        classname = find_class_name(file_content)
        namespaces = find_relevant_namespaces(file_content, classname)
        namespace = "::".join(namespaces)

        classname_with_namespace = ""
        if namespace != "":
            classname_with_namespace += namespace + "::"
        classname_with_namespace += classname

        ancestors = find_direct_public_ancestors(file_content)
        classes[classname_with_namespace] = {"file": file, "ancestors": ancestors}

    ancestor_files = []
    for ancestor in pick_relevant_ancestors(classes, class_name):
        if ancestor in classes:
            ancestor_files.append(classes.get(ancestor).get("file"))
    return ancestor_files


def collect_contents_of_ancestor_files(included_files, class_name):
    ancestors_files = pick_ancestors_files(included_files, class_name)

    content = ""
    for file in ancestors_files:
        file_content = load_and_preprocess_file(file)
        content += file_content
    return content


if __name__ == "__main__":
  import sys
  source_file = sys.argv[1]
  output_file = sys.argv[2]
  content = load_and_preprocess_file(source_file)
  class_name = find_class_name(content)
  namespace = determine_namespace(content, class_name)
  included_files = find_prerequisites(output_file, load_file_into_string("Makefile"))
  content += collect_contents_of_ancestor_files(included_files, namespace + "::" + class_name)
  functions = find_pure_virtual_functions(content)
  lookup_function_content = extract_lookup_function_content(functions)
  aliases = []
  aliases.extend(find_typedefs(content))
  aliases.extend(find_structs(content))
  aliases.extend(find_enums(content))
  
  with open(output_file, "w") as file:
    write_disclosure(file, sys.argv)
    write_includes(file, source_file)
    write_using_declaration(file, namespace, class_name)
    write_aliases(file, namespace, class_name, aliases)

    for identifier in lookup_function_content:
          ret_type, func_name, arguments, const_qualifier = lookup_function_content[identifier][0]
          arg_types = types_for_templates(arguments)
          file.write("int resolve_" + identifier + " (RET_TYPE (CLASSNAME::*memberFunction)(A1) CONST ) {\n"
            .replace("CLASSNAME", class_name)
            .replace("RET_TYPE",ret_type)
            .replace("A1",arg_types)
            .replace("CONST", const_qualifier))
          for i in range(len(lookup_function_content[identifier])):
            entry = lookup_function_content[identifier][i]
            ret_type, func_name, arguments, _ = entry
            file.write("  RET_TYPE (CLASSNAME::*fpNUMBER)(A1) CONST = &CLASSNAME::FUNCNAME;\n"
                .replace("CLASSNAME", class_name)
                .replace("RET_TYPE", ret_type)
                .replace("NUMBER", str(i))
                .replace("A1", arg_types)
                .replace("CONST", const_qualifier)
                .replace("FUNCNAME", func_name))
            file.write("  if (memberFunction == fp"+str(i)+") return "+str(i)+";\n")
          file.write("  throw NotAMemberFunction();\n")
          file.write("}\n\n")
    
    for identifier in lookup_function_content:
      ret_type, func_name, arguments, const_qualifier = lookup_function_content[identifier][0]
      arg_types = types_for_templates(arguments)
      file.write("typedef " + ret_type + " (" + class_name + "::*" + identifier + "_type)(" + arg_types + ") " + const_qualifier + ";\n")
    
    file.write("\n")
    file.write("template<>\nclass Mock<CLASSNAME>::Mock_impl {\npublic:\n".replace("CLASSNAME", class_name))
    file.write("  Mock_impl() {\n")
    for identifier in lookup_function_content:
      file.write("    for (int i = 0; i < {:d}".format(len(lookup_function_content[identifier])) + "; ++i) {\n")
      file.write("      " + identifier + "_functors[i] = 0;\n".format(len(lookup_function_content[identifier])))
      file.write("    }\n")
    file.write("  }\n")
    for identifier in lookup_function_content:
      ret_type, func_name, arguments, const_qualifier = lookup_function_content[identifier][0]
      arg_types = types_for_templates(arguments)
      arg_count = number_of_arguments(arg_types)
      file.write("    Func" + functor_signature(ret_type, arg_count, arg_types, const_qualifier))
      file.write("* " +  identifier + "_functors[{:d}];\n".format(len(lookup_function_content[identifier])))
    file.write("};\n")
    file.write("\n")
    file.write("template<> Mock<CLASSNAME>::Mock() {}\n".replace("CLASSNAME", class_name)) 
    file.write("template<> Mock<CLASSNAME>::~Mock() {}\n".replace("CLASSNAME", class_name)) 
    
    file.write("\n")
    file.write("class ActualMock : public Mock<CLASSNAME> {\n".replace("CLASSNAME", class_name))
    file.write("public:\n")
    file.write("  ActualMock() { d = new Mock<CLASSNAME>::Mock_impl();};\n".replace("CLASSNAME", class_name))
    file.write("  ~ActualMock() { delete d; };\n")
    for identifier in lookup_function_content:
      for i in range(len(lookup_function_content[identifier])):
        entry = lookup_function_content[identifier][i]
        ret_type, func_name, arguments, const_qualifier = entry
        arg_types = types_for_templates(arguments)
        arg_count = number_of_arguments(arg_types)
        
        file.write("  " + ret_type + " " + func_name + "(" + arguments + ") " + const_qualifier + " override {\n")
        file.write("    Func" + functor_signature(ret_type, arg_count, arg_types, const_qualifier))
        file.write("* functor = d->" + identifier + "_functors[{:d}];\n".format(i))
        file.write("    if (functor == 0) throw NoFunctionRegistered();\n")
        file.write("    return (*functor)" + "(" + arguments_for_calling(arguments) + ");\n  }\n\n")
          
    file.write("};\n\n")
    for identifier in lookup_function_content:
      ret_type, func_name, arguments, const_qualifier = lookup_function_content[identifier][0]
      arg_types = types_for_templates(arguments)
      arg_count = number_of_arguments(arg_types)
      file.write("""
template<>
template<>""")
      file.write("""
void Mock<CLASSNAME>::extend_internal(RET_TYPE (CLASSNAME::*memberFunction)(A1) CONST, Func1wR *functor) {
""".replace("CLASSNAME", class_name)
   .replace("RET_TYPE",ret_type)
   .replace("A1",arg_types)
   .replace("CONST", const_qualifier)
   .replace("<,","<")
   .replace("void>",">")
   .replace(",>",">")
   .replace("<>","")
   .replace("Func1wR","Func" + functor_signature(ret_type, arg_count, arg_types, const_qualifier)))
      file.write("  int index = resolve_" + identifier + "(memberFunction);\n")
      file.write("  d->" + identifier + "_functors[index] = functor;\n}\n")

    file.write("""

template<>
Mock<CLASSNAME>* Mock<CLASSNAME>::createMock() {
    return new ActualMock();
}

""".replace("CLASSNAME", class_name))
