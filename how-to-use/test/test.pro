QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

INCLUDEPATH += \
    ../code-to-test \
    ../../testdoublegenerator \

SOURCES += \
    test_ClassToTest.cpp \
    ../code-to-test/ClassToTest.cpp \

HEADERS += \
    ../code-to-test/ClassToTest.h \
    ../code-to-test/Dependency.h \
    ../../testdoublegenerator/mock_wrapper.h \

FILES_TO_WRAP += \
    ../code-to-test/Dependency.h

include(../../testdoublegenerator/generate_mocks.pri)
