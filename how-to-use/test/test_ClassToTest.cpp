#include <QtTest>

#include "mock_wrapper.h"

#include "ClassToTest.h"
#include "Dependency.h"

#include "Exceptions.h"

using ns1::ClassToTest;
using ns1::Dependency;

class test_ClassToTest : public QObject {
    Q_OBJECT

public:
    test_ClassToTest();
    ~test_ClassToTest();

private slots:
    void init();
    void cleanup();

    void throws_exception_if_invoked_method_is_not_registered();

    void calls_the_registered_lambda();
    void calls_the_registered_static_function();
    void calls_the_registered_member_function();

    void the_most_recent_extend_wins();

    void emits_signal_with_payload();

    void may_use_typedefs_as_arguments();
    void may_use_structs_as_arguments();
    void may_use_enums_as_arguments();

    void is_fine_with_additional_commas_in_signature();

    void extends_const_methods_as_well();

    void default_arguments_are_respected();

    void properly_resolves_overloaded_methods();

    void extends_methods_of_ancestors();

private:
    QSharedPointer<ClassToTest> uut;
    QSharedPointer<Mock<Dependency>> mock_Dependency;
};

test_ClassToTest::test_ClassToTest() {
}

test_ClassToTest::~test_ClassToTest() {
}

void test_ClassToTest::init() {
    mock_Dependency = QSharedPointer<Mock<Dependency>>(Mock<Dependency>::createMock());
    uut = QSharedPointer<ClassToTest>(new ClassToTest(mock_Dependency));
}

void test_ClassToTest::cleanup() {
    uut.reset();
    mock_Dependency.reset();
}

void test_ClassToTest::throws_exception_if_invoked_method_is_not_registered() {
    QVERIFY_EXCEPTION_THROWN(
        uut->callDoubleReturningMethodWithIntAndFloatArgument(1, 2), NoFunctionRegistered);
}

void test_ClassToTest::calls_the_registered_lambda() {
    const int intArg = 23;
    const float floatArg = 42.42;
    const double retValExpected = intArg + floatArg;

    mock_Dependency->extend<double, int, float>(
        &Dependency::doubleReturningMethodWithIntAndFloatArgument,
        [](const int firstArg, const float secondArg) -> double {
        return firstArg + secondArg;
    });

    QCOMPARE(uut->callDoubleReturningMethodWithIntAndFloatArgument(intArg, floatArg),
             retValExpected);
}

double staticFunction(const int intArg, const float floatArg) {
    return intArg + floatArg;
}

void test_ClassToTest::calls_the_registered_static_function() {
    const int intArg = 23;
    const float floatArg = 42.42;
    const double retValExpected = intArg + floatArg;

    mock_Dependency->extend(&Dependency::doubleReturningMethodWithIntAndFloatArgument,
                            staticFunction);

    QCOMPARE(uut->callDoubleReturningMethodWithIntAndFloatArgument(intArg, floatArg),
             retValExpected);
}

struct Foo {
    double memberFunction(const int intArg, const float floatArg) {
        return intArg + floatArg;
    }
};

void test_ClassToTest::calls_the_registered_member_function() {
    const int intArg = 23;
    const float floatArg = 42.42;
    const double retValExpected = intArg + floatArg;

    Foo foo;

    mock_Dependency->extend(&Dependency::doubleReturningMethodWithIntAndFloatArgument,
                            foo, &Foo::memberFunction);

    QCOMPARE(uut->callDoubleReturningMethodWithIntAndFloatArgument(intArg, floatArg),
             retValExpected);
}

void test_ClassToTest::the_most_recent_extend_wins() {
    mock_Dependency->extend<double, int, float>(
        &Dependency::doubleReturningMethodWithIntAndFloatArgument,
        [](const int, const float) ->double { return 1; });
    mock_Dependency->extend<double, int, float>(
        &Dependency::doubleReturningMethodWithIntAndFloatArgument,
        [](const int, const float) ->double { return 2; });

    QCOMPARE(uut->callDoubleReturningMethodWithIntAndFloatArgument(0, 0), 2);
}

void test_ClassToTest::emits_signal_with_payload() {
    const int payloadSent = 23;
    QSignalSpy spy(uut.data(), &ClassToTest::signalThatASignalWasReceived);

    emit mock_Dependency->signalWithIntPayload(payloadSent);

    const int payloadReceived = spy.takeFirst().at(0).toInt();
    QCOMPARE(payloadReceived, payloadSent);
}

void test_ClassToTest::may_use_typedefs_as_arguments() {
    Dependency::MyTypedef argument(23, 42.23);
    struct Bar {
        bool isCalled = false;
        void func(Dependency::MyTypedef arg) {
            if (arg == Dependency::MyTypedef(23, 42.23)) {
                isCalled = true;
            }
        }
    } bar;

    mock_Dependency->extend(&Dependency::methodUsingLocallyDefinedTypedefAsArgument, bar, &Bar::func);

    uut->callMethodUsingTypedef(argument);

    QVERIFY(bar.isCalled);
}

void test_ClassToTest::may_use_structs_as_arguments() {
    Dependency::MyStruct argument(23, true);
    struct Bar {
        bool isCalled = false;
        void func(Dependency::MyStruct arg) {
            if ((arg.i == 23) && (arg.b)) { isCalled = true; }
        }
    } bar;

    mock_Dependency->extend(&Dependency::methodUsingLocallyDefinedStructAsArgument, bar, &Bar::func);

    uut->callMethodUsingStruct(argument);

    QVERIFY(bar.isCalled);

}

void test_ClassToTest::may_use_enums_as_arguments() {
    Dependency::MyEnum argument(Dependency::MyEnum::B);
    struct Bar {
        bool isCalled = false;
        void func(Dependency::MyEnum arg) {
            if (arg == Dependency::MyEnum::B) { isCalled = true; }
        }
    } bar;

    mock_Dependency->extend(&Dependency::methodUsingLocallyDefinedEnumAsArgument, bar, &Bar::func);

    uut->callMethodUsingEnum(argument);

    QVERIFY(bar.isCalled);
}

void test_ClassToTest::is_fine_with_additional_commas_in_signature() {
    QPair<int, QPair<float, int>> arg1 = qMakePair(1, qMakePair(2., 3));
    QPair<QPair<int, int>, float> arg2 = qMakePair(qMakePair(3, 4), 5.);
    QPair<int, QPair<int, float>> retValExpected = qMakePair(1, qMakePair(3+4, 2.+5.));

    struct Bar {
        QPair<int, QPair<int, float>> func(QPair<int, QPair<float, int>> arg1, QPair<QPair<int, int>, float> arg2) {
            return qMakePair(arg1.first, qMakePair(arg1.second.second + arg2.first.second, arg1.second.first + arg2.second));
        }
    } bar;

    mock_Dependency->extend(&Dependency::methodWithCommasNotSeparatingArguments, bar, &Bar::func);

    QCOMPARE(uut->callMethodWithCommasNotSeparatingArguments(arg1, arg2), retValExpected);
}

void test_ClassToTest::extends_const_methods_as_well() {
    const bool arg = true;
    const bool retValExpected = !arg;

    struct Bar {
        bool func(bool arg) const { return !arg; }
    } bar;

    mock_Dependency->extend(&Dependency::constMethod, bar, &Bar::func);

    QCOMPARE(uut->callConstMethod(arg), retValExpected);
}

void test_ClassToTest::default_arguments_are_respected() {
    const int anyArgument = 0;
    const int retValExpected = 23;  // this is the default value of the default argument

    struct Bar {
        int func(int, int defaultArgument) const { return defaultArgument; }
    } bar;
    mock_Dependency->extend(&Dependency::methodWithDefaultArgument, bar, &Bar::func);

    QCOMPARE(uut->callMethodWithDefaultArgument(anyArgument), retValExpected);
}

void test_ClassToTest::properly_resolves_overloaded_methods() {
    const int intArg = 23;
    const float floatArg = 42.1;
    const QList<int> returnValueExpected = QList<int>()
                                           << 1 * intArg << 2 * intArg
                                           << 3 * floatArg << 4 * floatArg;

    struct Bar {
        int intIntConst(int arg) const { return 1 * arg; }
        int intInt(int arg) { return 2 * arg; }
        int intFloatConst(float arg) const { return 3 * arg; }
        float floatFloat(float arg) { return 4 * arg; }
    } bar;

    mock_Dependency->extend(&Dependency::anOverloadedMethod, bar, &Bar::intIntConst);
    mock_Dependency->extend(&Dependency::anOverloadedMethod, bar, &Bar::intInt);
    mock_Dependency->extend(&Dependency::anOverloadedMethod, bar, &Bar::intFloatConst);
    mock_Dependency->extend(&Dependency::anOverloadedMethod, bar, &Bar::floatFloat);

    QCOMPARE(uut->callOverloadedMethod(intArg, floatArg), returnValueExpected);
}

void test_ClassToTest::extends_methods_of_ancestors() {
    const int arg = 23;
    const QList<int> returnValueExpected = QList<int>()
                                           << 1 * arg
                                           << 2 * arg
                                           << 3 * arg;

    struct Bar {
        int parentAMethod(int arg) { return 1 * arg; }
        int parentBMethod(int arg) { return 2 * arg; }
        int grandParentMethod(int arg) { return 3 * arg; }
    } bar;

    int (Dependency::*parentAMethod)(int) = &ns3::ParentA::parentClassAMethod;
    int (Dependency::*parentBMethod)(int) = &ns1::ns2::ParentB::parentClassBMethod;
    int (Dependency::*grandParentMethod)(int) = &ns1::GrandParent::grandParentMethod;

    mock_Dependency->extend(parentAMethod, bar, &Bar::parentAMethod);
    mock_Dependency->extend(parentBMethod, bar, &Bar::parentBMethod);
    mock_Dependency->extend(grandParentMethod, bar, &Bar::grandParentMethod);

    QCOMPARE(uut->callMethodsOfAncestors(arg), returnValueExpected);
}



QTEST_APPLESS_MAIN(test_ClassToTest)

#include "test_ClassToTest.moc"
