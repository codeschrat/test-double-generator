#ifndef DEPENDENCY_H
#define DEPENDENCY_H

#include "ParentA.h"
#include "ParentB.h"
#include "ThirdParty.h"

#include <QObject>

namespace ns1 {
namespace ns2 {
class ForwardDeclaredClass;
}

class Dependency : public QObject, public ns3::ParentA, public ns1::ns2::ParentB {
    Q_OBJECT;

public:
    typedef QPair<int, float> MyTypedef;

    struct MyStruct {
        int i;
        bool b;
        MyStruct(int i, bool b) : i(i), b(b) {}
    };

    enum MyEnum {
        A,
        B,
    };

    virtual double doubleReturningMethodWithIntAndFloatArgument(int intArg, float floatArg) = 0;

    virtual void methodUsingLocallyDefinedTypedefAsArgument(MyTypedef pair) = 0;
    virtual void methodUsingLocallyDefinedStructAsArgument(MyStruct arg) = 0;
    virtual void methodUsingLocallyDefinedEnumAsArgument(MyEnum arg) = 0;

    virtual QList<int> methodWithAngleBracketsInSignature(QList<float> arg) = 0;

    virtual QPair<int, QPair<int, float>> methodWithCommasNotSeparatingArguments(QPair<int, QPair<float, int>> arg1,
                                                                                 QPair<QPair<int,int>, float> arg2) = 0;

    virtual bool constMethod(bool arg) const = 0;

    virtual int methodWithDefaultArgument(int arg1, int arg2 = 23) const = 0;

    virtual int anOverloadedMethod(int arg) const = 0;
    virtual int anOverloadedMethod(int arg) = 0;
    virtual int anOverloadedMethod(float arg) const = 0;
    virtual float anOverloadedMethod(float arg) = 0;

    virtual void methodUsingThirdPartyAsArgument(ThirdParty arg) = 0;

//    virtual void methodUsingPassByReferenceA(QString const& arg) = 0;
    virtual void methodUsingPassByReferenceB(const QString& arg) = 0;
    virtual void methodUsingPassByReferenceC(QString& arg) = 0;

signals:
    void signalWithIntPayload(int payload);
};

}

#endif // DEPENDENCY_H
