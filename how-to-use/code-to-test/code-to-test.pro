QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

SOURCES += \
        ClassToTest.cpp \
        main.cpp \

HEADERS += \
    ClassToTest.h \
    Dependency.h \
    ParentA.h \
    ParentB.h \
    GrandParent.h \
    ThirdParty.h \
