#ifndef GRANDPARENT_H
#define GRANDPARENT_H

namespace ns1 {
class GrandParent {
public:
    virtual int grandParentMethod(int arg) = 0;
};
}

#endif // GRANDPARENT_H
