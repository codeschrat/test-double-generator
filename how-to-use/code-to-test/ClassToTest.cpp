#include "ClassToTest.h"

namespace ns1 {

ClassToTest::ClassToTest(QSharedPointer<Dependency> dependency)
    : QObject()
    , dependency(dependency) {

    connect(dependency.data(), &Dependency::signalWithIntPayload,
            this, &ClassToTest::signalThatASignalWasReceived);
}

double ClassToTest::callDoubleReturningMethodWithIntAndFloatArgument(const int intArg,
                                                                     const float floatArg) {
    return dependency->doubleReturningMethodWithIntAndFloatArgument(intArg, floatArg);
}

void ClassToTest::callMethodUsingTypedef(Dependency::MyTypedef arg) {
    dependency->methodUsingLocallyDefinedTypedefAsArgument(arg);
}

void ClassToTest::callMethodUsingStruct(Dependency::MyStruct arg) {
    dependency->methodUsingLocallyDefinedStructAsArgument(arg);
}

void ClassToTest::callMethodUsingEnum(Dependency::MyEnum arg) {
    dependency->methodUsingLocallyDefinedEnumAsArgument(arg);
}

QPair<int, QPair<int, float>> ClassToTest::callMethodWithCommasNotSeparatingArguments(
        QPair<int, QPair<float, int>> arg1, QPair<QPair<int,int>, float> arg2) {
    return dependency->methodWithCommasNotSeparatingArguments(arg1, arg2);
}

bool ClassToTest::callConstMethod(bool arg) {
    return dependency->constMethod(arg);
}

int ClassToTest::callMethodWithDefaultArgument(int arg) {
    return dependency->methodWithDefaultArgument(arg);
}

QList<int> ClassToTest::callOverloadedMethod(int arg1, float arg2) {

    int (Dependency::*intIntConst)(int) const = &Dependency::anOverloadedMethod;
    int (Dependency::*intInt)(int) = &Dependency::anOverloadedMethod;
    int (Dependency::*intFloatConst)(float) const = &Dependency::anOverloadedMethod;
    float (Dependency::*floatFloat)(float) = &Dependency::anOverloadedMethod;

    QList<int> retVal = QList<int>()
                        << (dependency.data()->*intIntConst)(arg1)
                        << (dependency.data()->*intInt)(arg1)
                        << (dependency.data()->*intFloatConst)(arg2)
                        << (dependency.data()->*floatFloat)(arg2);

    return retVal;
}

QList<int> ClassToTest::callMethodsOfAncestors(const int arg) {
    const QList<int> returnValue = QList<int>()
                                   << dependency->parentClassAMethod(arg)
                                   << dependency->parentClassBMethod(arg)
                                   << dependency->grandParentMethod(arg);
    return returnValue;
}

void ClassToTest::passArgumentByReference(QString arg) {
//    dependency->methodUsingPassByReferenceA(arg);
    dependency->methodUsingPassByReferenceB(arg);
    dependency->methodUsingPassByReferenceC(arg);
}

}
